import UIKit

class LoginScreenModuleConfigurator {
    
    @discardableResult
    func configure(viewController: UIViewController, output moduleOutput: LoginScreenModuleOutput?) -> LoginScreenModuleInput? {
        guard let viewController = viewController as? LoginScreenViewController else { return nil }

        let router = LoginScreenRouter()
        router.viewController = viewController

        let presenter = LoginScreenPresenter()
        presenter.view = viewController
        presenter.router = router
        presenter.moduleOutput = moduleOutput
        presenter.validationService = ServiceFactory.shared.validationService

        let moduleInput = presenter

        let interactor = LoginScreenInteractor()
        interactor.output = presenter
        interactor.apiService = ServiceFactory.shared.apiService
        interactor.realmService = ServiceFactory.shared.realmService
        interactor.tokenStorage = ServiceFactory.shared.storageService
        interactor.socketService = ServiceFactory.shared.socketService

        presenter.interactor = interactor
        viewController.output = presenter
        
        let tableViewAdapter = TableViewAdapter()
        let mainSectionAdapter = CellsSetSectionAdapter()
        
        let phoneCellAdapter = PhoneTextFieldCellAdapter(output: presenter)
        let passwordCellAdapter = PasswordTextFieldCellAdapter(output: presenter, state: .new)
        let registerCellAdapter = UnderlineButtonCellAdapter(output: presenter, type: .register)
        let loginButtonCellAdapter = VioletButtonCellAdapter(output: presenter)
        let forgotPassCellAdapter = UnderlineButtonCellAdapter(output: presenter, type: .forgot)
        
        mainSectionAdapter.cellAdapters = [phoneCellAdapter, passwordCellAdapter, registerCellAdapter, loginButtonCellAdapter, forgotPassCellAdapter]
        tableViewAdapter.sectionAdapters = [mainSectionAdapter]
        viewController.tableViewAdapter = tableViewAdapter
        
        return moduleInput
    }
}
