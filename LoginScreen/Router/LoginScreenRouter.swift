import UIKit

protocol LoginScreenRouterInput: ShowAlertScreenRouter, ShowProgressHUDRouterProtocol, ShowTabBarRouterProtocol, ShowOnBoardingScreenProtocol {
    func showPassRecoverScreen()
}

class LoginScreenRouter {
	weak var viewController: UIViewController!
}

// MARK: - LoginScreenRouterInput
extension LoginScreenRouter: LoginScreenRouterInput {
    func showPassRecoverScreen() {
        let vc = PassRecoverScreenViewController.instantiateFromStoryboard()
        let configurator = PassRecoverScreenModuleConfigurator()
        
        configurator.configure(viewController: vc, output: nil)
        viewController.setEmptyBackButton()
        
        viewController.navigationController?.pushViewController(vc, animated: true)
    }
}
