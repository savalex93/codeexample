import UIKit

protocol LoginScreenViewInput: class {
    func setupInitialState()
    func reloadTable()
}

protocol LoginScreenViewOutput {
    func viewIsReady()
}

class LoginScreenViewController: UIViewController {
    var output: LoginScreenViewOutput!
    
    @IBOutlet private weak var welcomeLabel: UILabel!
    @IBOutlet private weak var roundView: UIView!
    @IBOutlet private weak var tableView: UITableView!
    
    var tableViewAdapter: TableViewAdapterInput!

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.isScrollEnabled = tableView.contentSize.height > roundView.frame.height
    }
}

// MARK: - LoginScreenViewInput
extension LoginScreenViewController: LoginScreenViewInput {
    func setupInitialState() {
        setupText()
        setupUI()
        self.addTapGestureRecognizer()
        
        tableViewAdapter.setup(tableView)
    }
    
    func reloadTable() {
        tableView.reloadData()
    }
}

// MARK: - Private
private extension LoginScreenViewController {
    func setupText() {
        welcomeLabel.text = "screen.login.welcome.text".localized()
    }
    
    func setupUI() {
        tableView.layer.cornerRadius = 25
        roundView.layer.cornerRadius = 25
    }
}
