import Foundation
import UIKit.NSAttributedString

protocol LoginScreenModuleInput: class {}

protocol LoginScreenModuleOutput: class {}

class LoginScreenPresenter {
	weak var moduleOutput: LoginScreenModuleOutput?

	var router: LoginScreenRouterInput!

	var interactor: LoginScreenInteractorInput!
	
    weak var view: LoginScreenViewInput!
    
    var validationService: ValidationServiceInput!
    
    private var progressHUDModuleInput: ProgressHUDModuleInput?
    
    private var isNeedToValidate = false
    
    private var phone: String?
    private var password: String?
}

// MARK: - LoginScreenViewOutput
extension LoginScreenPresenter: LoginScreenViewOutput {
    func viewIsReady() {
        view.setupInitialState()
    }
}

// MARK: - LoginScreenInteractorOutput
extension LoginScreenPresenter: LoginScreenInteractorOutput {
    func loginSuccess() {
        progressHUDModuleInput?.hide {
            self.router.showMainScreen()
        }
    }
    
    func loginDidFail(_ error: InteractorError) {
        progressHUDModuleInput?.hide {
            self.router.showAlert(for: error, completion: nil)
        }
    }
}

// MARK: - LoginScreenModuleInput
extension LoginScreenPresenter: LoginScreenModuleInput {}

// MARK: - PhoneTextFieldCellAdapterOutput
extension LoginScreenPresenter: PhoneTextFieldCellAdapterOutput {
    func phoneTextFieldDisplayModel() -> TextFieldCellDisplayModel {
        return TextFieldCellDisplayModel(value: String.format(phoneNumber: phone ?? ""),
                                         placeholder: "screen.login.phone.placeholder".localized(),
                                         isErrorState: isNeedToValidate ? !validationService.isPhoneValid(phone) : false, isEditable: true)
    }
    
    func phoneDidChange(_ phone: String) {
        self.phone = phone
    }
}

// MARK: - PasswordTextFieldCellAdapterOuput
extension LoginScreenPresenter: PasswordTextFieldCellAdapterOuput {
    func passwordTextFieldDisplayModel(for state: PasswordState) -> TextFieldCellDisplayModel {
        return TextFieldCellDisplayModel(value: password,
                                         placeholder: "screen.login.password.placeholder".localized(),
                                         isErrorState: isNeedToValidate ? !validationService.isPasswordValid(password) : false, isEditable: true)
    }
    
    func passwordDidChange(_ password: String?, state: PasswordState) {
        self.password = password
    }
}

// MARK: - UnderlineButtonCellAdapterOutput
extension LoginScreenPresenter: UnderlineButtonCellAdapterOutput {
    func underLineButtonDisplayModel(for type: UnderlineButtonType) -> UnderlinedButtonCellDisplayModel? {
        switch type {
        case .register:
            return UnderlinedButtonCellDisplayModel(title: NSAttributedString(string: "screen.login.button.register".localized(),
                                                                              attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue, .foregroundColor: #colorLiteral(red: 0.456004262, green: 0.4560155272, blue: 0.4560095072, alpha: 1)]))
        case .forgot:
            return UnderlinedButtonCellDisplayModel(title: NSAttributedString(string: "screen.login.button.forgot".localized(),
            attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue, .foregroundColor: #colorLiteral(red: 0.456004262, green: 0.4560155272, blue: 0.4560095072, alpha: 1)]))
        default:
            return nil
        }
    }
    
    func underLineButtonTapped(with type: UnderlineButtonType) {
        switch type {
        case .register:
            router.showOnBoardingScreen(with: .first)
        case .forgot:
            router.showPassRecoverScreen()
        default:
            break
        }
    }
}

// MARK: - VioletButtonCellAdapterOutput
extension LoginScreenPresenter: VioletButtonCellAdapterOutput {
    func violetButtonDisplayModel() -> VioletButtonDisplayModel {
        return VioletButtonDisplayModel(title: "screen.login.button.login".localized())
    }
    
    func violetButtonTapped() {
        isNeedToValidate = true
        
        guard validationService.isPhoneValid(phone), let phone = phone?.dropFirst(), validationService.isPasswordValid(password) else {
            view.reloadTable()
            
            return
        }
        
        progressHUDModuleInput = router.presentProgressHUD()
        
        interactor.login(with: String(phone), password: password!)
    }
}
