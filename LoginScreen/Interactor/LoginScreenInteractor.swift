import Foundation

protocol LoginScreenInteractorInput {
    func login(with phone: String, password: String)
}

protocol LoginScreenInteractorOutput: class {
    func loginSuccess()
    func loginDidFail(_ error: InteractorError)
}

class LoginScreenInteractor {
    weak var output: LoginScreenInteractorOutput!
    
    typealias TokenStorageInput = TokensStorageInput & FirebaseStorageInput
    var apiService: LoginAPIServiceInput!
    var realmService: RealmServiceInput!
    var tokenStorage: TokenStorageInput!
    var socketService: SocketServiceInput!
}

// MARK: - LoginScreenInteractorInput
extension LoginScreenInteractor: LoginScreenInteractorInput {
    func login(with phone: String, password: String) {
        if Connectivity.isConnectedToInternet {
            apiService.login(with: phone, password: password) { [self] result in
                
                switch result {
                case .success(let info):
                    guard let token = info.token else { return }
                    
                    let profileRealmObject = ProfileRealmObj(apiResult: info.userInfo)
                    realmService.save(profileRealmObject, update: true)
                    tokenStorage.saveToken(token.token)
                    tokenStorage.saveRefresh(token.refresh)
                    
                    output.loginSuccess()
                case .failure(let error):
                    switch error {
                    case .negativeStatus(let error):
                        output.loginDidFail(.unableToProceed(error: error))
                    default:
                        output.loginDidFail(.unknown)
                    }
                }
            }
        } else {
            output.loginDidFail(.noInternet)
        }
    }
}
